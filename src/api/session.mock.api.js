import delay from './delay';

const AUTHENTICATED_USER = {
  email: 'user@example.com',
  password: 'password'
};

const TOKEN = 'JWT_TOKEN';

class SessionApi {
  static getToken({ email, password }) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (email === AUTHENTICATED_USER.email && password === AUTHENTICATED_USER.password) {
          resolve(TOKEN);
        }
        else {
          reject('Authorization failed.');
        }
      }, delay);
    });
  }
}

export default SessionApi;

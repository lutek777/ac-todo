import React from 'react';

class LoginBox extends React.Component {

  constructor(props) {
    super(props);

    this.state = {email: '', password: ''};

    this.onTextChange = this.onTextChange.bind(this);
    this.login = this.login.bind(this);
  }

  onTextChange(event) {
    const {value, type} = event.target;

    this.setState({[type]: value});
  }

  login(event) {
    event.preventDefault();
    this.props.loginUser(this.state);
  }

  render() {
    const {email, password} = this.state;
    const {isLoading} = this.props;

    return (
      <div className="panel panel-default login-box" style={{marginTop: '10rem'}}>
        <div className="panel-body text-center">
          <form onSubmit={this.login}>
            <div className="form-group">
              <input type="email"
                     className="form-control js-email"
                     id="exampleInputEmail1"
                     value={email}
                     onChange={this.onTextChange}
                     placeholder="Email"/>
            </div>

            <div className="form-group">
              <input type="password"
                     className="form-control js-password"
                     id="exampleInputPassword1"
                     value={password}
                     onChange={this.onTextChange}
                     placeholder="Password"/>
            </div>

            <button type="submit"
                    className="btn btn-primary"
                    disabled={isLoading}>{isLoading ? 'Submiting...' : 'Submit'}</button>
          </form>
        </div>
      </div>
    );
  }
}

LoginBox.propTypes = {
  loginUser: React.PropTypes.func.isRequired,
  isLoading: React.PropTypes.bool
};

export default LoginBox;

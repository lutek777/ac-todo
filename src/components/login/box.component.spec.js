import React from 'react';
import {shallow} from 'enzyme';
import LoginBox from './box.component';

let wrapper;

const loginUserSpy = jest.fn();
const TEXT = '__TEXT__';
const TEXT_2 = '__TEXT_2__';
const TEXT_3 = '__TEXT_3__';

function setup(isLoading = false) {
  const props = {
    isLoading,
    loginUser: loginUserSpy
  };

  return shallow(<LoginBox {...props} />)
}

describe('Todo Actions Component', () => {
  beforeEach(() => {
    wrapper = setup();
  });

  it('should renders properly', () => {
    expect(wrapper.find('.login-box').length).toBe(1);
  });

  it('should set state on text change', () => {
    wrapper.find('.js-email').simulate('change', {
      target: { value: TEXT, type: 'email' }
    });

    expect(wrapper.state().email).toEqual(TEXT);
    expect(loginUserSpy).not.toHaveBeenCalled();
  });

  it('should set state on text change', () => {
    wrapper.find('.js-password').simulate('change', {
      target: { value: TEXT_2, type: 'password' }
    });

    expect(wrapper.state().password).toEqual(TEXT_2);
    expect(loginUserSpy).not.toHaveBeenCalled();
  });

  it('should call loginUser on form submit', () => {
    wrapper.setState({ email: TEXT_2, password: TEXT_3 });
    wrapper.find('form').simulate('submit', { preventDefault: jest.fn() });

    expect(loginUserSpy).toHaveBeenCalledWith({
      email: TEXT_2,
      password: TEXT_3
    });
  });

  it('should handle loading state', () => {
    const wrapper_loadering = setup(true);
    const button = wrapper_loadering.find('button');

    expect(button.text()).toEqual('Submiting...');
    expect(button.props().disabled).toBeTruthy;
  });
});

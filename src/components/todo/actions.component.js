import React from 'react';

// TODO: clunky but fast way, probably would be better to keep it in store
let sortByNameCounter = 0;
let sortByStatusCounter = 0;

const TodoActions = (props) => {
  const sortByName = () => {
    props.sortByName(sortByNameCounter++ % 2);
  };

  const sortByStatus = () => {
    props.sortByStatus(sortByStatusCounter++ % 2);
  };

  return (
    <div className="list-group">
      <button className="list-group-item" onClick={props.startAddingItem}>add todo</button>
      <button className="list-group-item" onClick={props.hideDone}>hide done</button>
      <button className="list-group-item js-sort-name" onClick={sortByName}>sort by name</button>
      <button className="list-group-item js-sort-status" onClick={sortByStatus}>sort by status</button>
    </div>
  );
};

TodoActions.propTypes = {
  sortByName: React.PropTypes.func.isRequired,
  sortByStatus: React.PropTypes.func.isRequired,
  startAddingItem: React.PropTypes.func.isRequired,
  hideDone: React.PropTypes.func.isRequired
};

export default TodoActions;

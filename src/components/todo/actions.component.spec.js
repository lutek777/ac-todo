import React from 'react';
import {shallow} from 'enzyme';
import TodoActions from './actions.component';

let wrapper;

const props = {
  startAddingItem: jest.fn(),
  hideDone: jest.fn(),
  sortByName: jest.fn(),
  sortByStatus: jest.fn()
};

function setup() {
  return shallow(<TodoActions {...props} />)
}

describe('Todo Actions Component', () => {
  beforeEach(() => {
    wrapper = setup();
  });

  it('should render properly', () => {
    expect(wrapper.find('.list-group-item').length).toBe(4);
  });

  it('should call sortByName with values in turns', () => {
    wrapper.find('.js-sort-name').simulate('click');
    expect(props.sortByName).toHaveBeenCalledWith(0);
    wrapper.find('.js-sort-name').simulate('click');
    expect(props.sortByName).toHaveBeenCalledWith(1);
    wrapper.find('.js-sort-name').simulate('click');
    expect(props.sortByName).toHaveBeenCalledWith(0);
  });

  it('should call sortByStatus with values in turns', () => {
    wrapper.find('.js-sort-status').simulate('click');
    expect(props.sortByStatus).toHaveBeenCalledWith(0);
    wrapper.find('.js-sort-status').simulate('click');
    expect(props.sortByStatus).toHaveBeenCalledWith(1);
    wrapper.find('.js-sort-status').simulate('click');
    expect(props.sortByStatus).toHaveBeenCalledWith(0);
  });
});

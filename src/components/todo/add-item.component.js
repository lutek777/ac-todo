import React from 'react';

class TodoAddItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: ''
    };

    this.onTextChange = this.onTextChange.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.addTodo = this.addTodo.bind(this);
  }

  onTextChange(event) {
    this.setState({ text: event.target.value });
  }

  onKeyPress(event) {
    if (event.key === 'Enter') {
      this.addTodo();
    }
  }

  addTodo() {
    this.props.addItem(this.state.text);
  }

  render() {
    return (
      <div className="input-group">
        <input type="text"
               className="form-control"
               value={this.state.text}
               onChange={this.onTextChange}
               onKeyPress={this.onKeyPress}
               autoFocus/>
        <div className="input-group-btn">
          <button type="button" className="btn btn-default" onClick={this.addTodo}>
            <span className="glyphicon glyphicon-plus"></span>
          </button>
        </div>
      </div>
    );
  }

}

TodoAddItem.propTypes = {
  addItem: React.PropTypes.func.isRequired
};

export default TodoAddItem;

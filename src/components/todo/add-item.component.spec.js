import React from 'react';
import {shallow} from 'enzyme';
import TodoAddItem from './add-item.component';

let wrapper;

const TEXT = '__TEXT__';
const TEXT_2 = '__TEXT_2__';
const TEXT_3 = '__TEXT_3__';
const props = {
  addItem: jest.fn()
};

function setup() {
  return shallow(<TodoAddItem {...props} />)
}

describe('Todo Actions Component', () => {
  beforeEach(() => {
    wrapper = setup();
  });

  it('should renders properly', () => {
    expect(wrapper.find('.input-group').length).toBe(1);
  });

  it('should set state on text change', () => {
    wrapper.find('input').simulate('change', { target: { value: TEXT } });
    expect(wrapper.state().text).toEqual(TEXT);
    expect(props.addItem).not.toHaveBeenCalled();
  });

  it('should not call addItem on non-enter keys pressed', () => {
    wrapper.setState({ text: TEXT_2 });
    wrapper.find('input').simulate('keypress', {key: 'a'});

    expect(props.addItem).not.toHaveBeenCalled();
  });

  it('should call addItem on enter key pressed', () => {
    wrapper.setState({ text: TEXT_2 });
    wrapper.find('input').simulate('keypress', {key: 'Enter'});

    expect(props.addItem).toHaveBeenCalledWith(TEXT_2);
  });

  it('should call addItem on button click', () => {
    wrapper.setState({ text: TEXT_3 });
    wrapper.find('button').simulate('click');

    expect(props.addItem).toHaveBeenCalledWith(TEXT_3);
  });
});

import React from 'react';

class TodoListItem extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      text: props.todo.text
    };

    this.onTextChange = this.onTextChange.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.toggleTodo = this.toggleTodo.bind(this);
    this.updateTodo = this.updateTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { text } = nextProps.todo;

    if (this.props.todo.text !== text) {
      this.setState({ text });
    }
  }

  onTextChange(event) {
    this.setState({ text: event.target.value });
  }

  onKeyPress(event) {
    if (event.key === 'Enter') {
      this.updateTodo();
      event.target.blur();
    }
  }

  updateTodo() {
    const { text } = this.state;
    const { done } = this.props.todo;

    this.props.editItem(this.props.index, { done, text });
  }

  toggleTodo() {
    const {done, text} = this.props.todo;
    // TODO: consider handling this one with separate action type
    this.props.editItem(this.props.index, { done: !done, text });
  }

  deleteTodo() {
    this.props.deleteItem(this.props.index);
  }

  get isHidden() {
    return this.props.hideDone && this.props.todo.done;
  }

  render() {
    const {done} = this.props.todo;

    return (
      <div className={'input-group' + (this.isHidden ? ' hidden' : '') }>
        <input type="text"
               className="form-control"
               value={this.state.text}
               onChange={this.onTextChange}
               onBlur={this.updateTodo}
               onKeyPress={this.onKeyPress}
               disabled={done}
        />
        <div className="input-group-btn">
          <button type="button" className="btn btn-default js-toggle" onClick={this.toggleTodo}>
            <span className={'glyphicon glyphicon-' + (done ? 'check' : 'unchecked')}></span>
          </button>
          <button type="button" className="btn btn-default js-delete" onClick={this.deleteTodo}>
            <span className="glyphicon glyphicon-trash"></span>
          </button>
        </div>
      </div>
    );
  }
}

TodoListItem.propTypes = {
  todo: React.PropTypes.object.isRequired,
  index: React.PropTypes.number.isRequired,
  editItem: React.PropTypes.func.isRequired,
  deleteItem: React.PropTypes.func.isRequired,
  hideDone: React.PropTypes.bool.isRequired
};

export default TodoListItem;

import React from 'react';
import {shallow} from 'enzyme';
import TodoListItem from './list-item.component';

const blurSpy = jest.fn();
const editItemSpy = jest.fn();
const deleteItemSpy = jest.fn();
const TEXT = '__TEXT__';
const TEXT_2 = '__TEXT_2__';
const TEXT_3 = '__TEXT_3__';
const TEXT_4 = '__TEXT_4__';
const INDEX = 123;

function setup({ hideDone = false, done = false }) {
  const props = {
    todo: { text: TEXT, done },
    index: INDEX,
    hideDone,
    editItem: editItemSpy,
    deleteItem: deleteItemSpy
  };

  return shallow(<TodoListItem {...props} />)
}

describe('Todo Actions Component', () => {
  beforeEach(() => {

  });

  it('should renders properly', () => {
    let wrapper = setup({});

    expect(wrapper.find('.input-group').length).toBe(1);
  });

  it('should set state on text change', () => {
    let wrapper = setup({});

    wrapper.find('input').simulate('change', { target: { value: TEXT } });
    expect(wrapper.state().text).toEqual(TEXT);
    expect(editItemSpy).not.toHaveBeenCalled();
  });

  it('should not call editItem on non-enter keys pressed', () => {
    let wrapper = setup({});

    wrapper.setState({ text: TEXT_2 });
    wrapper.find('input').simulate('keypress', {key: 'a'});

    expect(editItemSpy).not.toHaveBeenCalled();
  });

  it('should call editItem on enter key pressed', () => {
    let wrapper = setup({});

    wrapper.setState({ text: TEXT_3 });
    wrapper.find('input').simulate('keypress', {
      key: 'Enter',
      target: { blur: blurSpy }
    });

    expect(editItemSpy).toHaveBeenCalledWith(INDEX, {
      text: TEXT_3,
      done: false
    });
  });

  it('should call editItem on toggle button clicked', () => {
    let wrapper = setup({});

    wrapper.find('.js-toggle').simulate('click');

    expect(editItemSpy).toHaveBeenCalledWith(INDEX, {
      text: TEXT,
      done: true
    });

  });

  it('should call deleteItem on delete button clicked', () => {
    let wrapper = setup({});

    wrapper.find('.js-delete').simulate('click');

    expect(deleteItemSpy).toHaveBeenCalledWith(INDEX);
  });

  it('should render check icon for done todo', () => {
    let wrapper = setup({});

    expect(wrapper.find('.glyphicon-check').length).toBe(0);

    wrapper = setup({ done: true });

    expect(wrapper.find('.glyphicon-check').length).toBe(1);
  });

  it('should hide whole list item if hideDone is passed and item is done', () => {
    let wrapper = setup({ done: true, hideDone: true });

    expect(wrapper.find('.hidden').length).toBe(1);
  });

  it('should not hide whole list item if hideDone is passed but item is not done', () => {
    let wrapper = setup({ hideDone: true });

    expect(wrapper.find('.hidden').length).toBe(0);
  });

  it('should update state text if it comes with props', () => {
    let wrapper = setup({});
    wrapper.setProps({ todo: { text: TEXT_4 } });

    expect(wrapper.state().text).toEqual(TEXT_4);
  });
});

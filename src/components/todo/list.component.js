import React from 'react';
import TodoListItem from './list-item.component';
import TodoAddItem from './add-item.component';

const TodoList = ({ todos, hideDone, isAdding, editItem, deleteItem, addItem }) => {
  return (
    <ul className="list-group">
      {todos.map((todo, index) =>
        <TodoListItem key={index}
                      todo={todo}
                      index={index}
                      hideDone={hideDone}
                      editItem={editItem}
                      deleteItem={deleteItem} />
      )}
      { isAdding ? (<TodoAddItem addItem={addItem}></TodoAddItem>) : ''}
    </ul>
  );
};

TodoList.propTypes = {
  todos: React.PropTypes.array.isRequired,
  hideDone: React.PropTypes.bool.isRequired,
  isAdding: React.PropTypes.bool,
  editItem: React.PropTypes.func.isRequired,
  deleteItem: React.PropTypes.func.isRequired,
  addItem: React.PropTypes.func.isRequired
};

export default TodoList;

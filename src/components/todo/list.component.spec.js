import React from 'react';
import {shallow, mount} from 'enzyme';
import TodoList from './list.component';

function setup({ todos = [], isAdding = false }, isFullRender = false) {
  const props = {
    todos,
    isAdding,
    hideDone: false,
    editItem: jest.fn(),
    deleteItem: jest.fn(),
    addItem: jest.fn()
  };
  const render = isFullRender ? mount : shallow;

  return render(<TodoList {...props} />)
}

describe('Todo List Component', () => {
  it('should render properly', () => {
    const wrapper = setup({});

    expect(wrapper.find('.list-group').length).toBe(1);
  });

  it('should render list items if todos available', () => {
    const wrapper = setup({
      todos: [
        { todo: '', index: 0 },
        { todo: '', index: 1 }
      ]
    }, true);

    expect(wrapper.find('.input-group').length).toBe(2);
  });

  it('should render add item when props passed', () => {
    const wrapper = setup({
      isAdding: true
    }, true);

    expect(wrapper.find('.input-group').length).toBe(1);

  });
});

import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import LoginBox from '../components/login/box.component';
import * as actions from '../store/session/session.actions';

class LoginPage extends React.Component {
  componentWillReceiveProps(nextProps) {
    const {shouldRedirect} = nextProps;
    if (shouldRedirect !== this.props.shouldRedirect && shouldRedirect === true) {
      this.context.router.push('/');
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-offset-3 col-sm-6">
            <LoginBox loginUser={this.props.actions.loginUser} isLoading={this.props.isLoading} />
          </div>
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  actions: React.PropTypes.object.isRequired,
  isLoading: React.PropTypes.bool,
  shouldRedirect: React.PropTypes.bool
};

LoginPage.contextTypes = {
  router: React.PropTypes.object
};

function mapStateToProps(state) {
  return {
    isLoading: state.session.isLoading,
    shouldRedirect: state.session.shouldRedirect
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

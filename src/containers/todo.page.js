import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import TodoActions from '../components/todo/actions.component';
import TodoList from '../components/todo/list.component';
import * as actions from '../store/todo/todo.actions';

export const TodoPage = (props) => {

  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-4">
          <TodoActions startAddingItem={props.actions.startAddingItem}
                       hideDone={props.actions.hideDone}
                       sortByName={props.actions.sortByName}
                       sortByStatus={props.actions.sortByStatus}>
          </TodoActions>
        </div>
        <div className="col-sm-8">
          <TodoList todos={props.todos}
                    isAdding={props.isAdding}
                    hideDone={props.hideDone}
                    addItem={props.actions.addItem}
                    deleteItem={props.actions.deleteItem}
                    editItem={props.actions.editItem}>
          </TodoList>
        </div>
      </div>
    </div>
  );
};

TodoPage.propTypes = {
  actions: React.PropTypes.object.isRequired,
  todos: React.PropTypes.array.isRequired,
  isAdding: React.PropTypes.bool,
  hideDone: React.PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isAdding: state.todo.isAdding,
    hideDone: state.todo.hideDone,
    todos: state.todo.items
  };
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoPage);

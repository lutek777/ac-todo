import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory,  } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from './store';
import TodoPage from './containers/todo.page';
import LoginPage from './containers/login.page';
import './index.css';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

function isLoggedIn() {
  const {token} = store.getState().session;

  return !!token;
}

function onEnter(nextState, replace) {
  const {pathname} = nextState.location;
  const isLogged = isLoggedIn();

  if (isLogged && pathname === '/login') {
    replace('/');
  }
  else if (!isLogged && pathname === '/') {
    replace('/login');
  }
}

const Routes = props => (
  <Router {...props}>
    <Route path="/" component={TodoPage} onEnter={onEnter} />
    <Route path="/login" component={LoginPage} onEnter={onEnter} />
  </Router>
);

render(
  <Provider store={store}>
    <Routes history={history} />
  </Provider>, document.getElementById('root')
);

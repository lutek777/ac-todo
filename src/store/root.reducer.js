import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import todo from './todo/todo.reducer';
import session from './session/session.reducer';

const rootReducer = combineReducers({
  todo,
  session,
  routing: routerReducer
});

export default rootReducer;

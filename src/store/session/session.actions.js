import * as types from './action-types';
import sessionApi from '../../api/session.mock.api';

export function loginUser(credentials) {
  return dispatch => {
    const {email} = credentials;

    dispatch({ type: types.SESSION_LOGIN });

    return sessionApi.getToken(credentials).then(token => {
      dispatch({ type: types.SESSION_LOGIN_SUCCESS, email, token });
    }).catch(error => {
      dispatch({ type: types.SESSION_LOGIN_FAIL, error });
    });
  };
}

export function logoutUser() {
  return { type: types.SESSION_LOGOUT };
}

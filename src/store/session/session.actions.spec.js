import * as types from './action-types';
import * as actions from './session.actions';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

let store;

const mockStore = configureMockStore([thunk]);
const AUTHORIZED_CREDENTIALS = {
  email: 'user@example.com',
  password: 'password'
};
const UNAUTHORIZED_CREDENTIALS = {
  email: '__EMAIL__',
  password: '__PASSWORD__'
};

describe('Session Actions', () => {
  describe('loginUser', () => {
    beforeEach(() => {
      store = mockStore({session: {}});
    });

    it('should create SESSION_LOGIN and SESSION_LOGIN_SUCCESS actions on successful login', () => {
      return store.dispatch(actions.loginUser(AUTHORIZED_CREDENTIALS)).then(() => {
        const storeActions = store.getActions();

        expect(storeActions[0]).toEqual({
          type: types.SESSION_LOGIN
        });
        expect(storeActions[1]).toEqual({
          type: types.SESSION_LOGIN_SUCCESS,
          email: AUTHORIZED_CREDENTIALS.email,
          token: 'JWT_TOKEN' // comes from mocked api
        });
      });
    });

    it('should create SESSION_LOGIN and SESSION_LOGIN_FAIL actions on successful login', () => {
      return store.dispatch(actions.loginUser(UNAUTHORIZED_CREDENTIALS)).then(() => {
        const storeActions = store.getActions();

        expect(storeActions[0]).toEqual({
          type: types.SESSION_LOGIN
        });
        expect(storeActions[1]).toEqual({
          type: types.SESSION_LOGIN_FAIL,
          error: 'Authorization failed.' // comes from mocked api
        });
      });
    });

  });

  describe('logoutUser', () => {
    it('should create SESSION_LOGOUT action', () => {
      expect(actions.logoutUser()).toEqual({
        type: types.SESSION_LOGOUT
      });
    });
  });
});

import * as types from './action-types';

export const INITIAL_STATE = {
  token: '',
  email: '',
  isLoading: false,
  hasError: false,
  shouldRedirect: false
};

function sessionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.SESSION_LOGIN:
      return Object.assign({}, state, { isLoading: true });

    case types.SESSION_LOGIN_SUCCESS:
      return {
        token: action.token,
        email: action.email,
        isLoading: false,
        hasError: false,
        shouldRedirect: true
      };

    case types.SESSION_LOGIN_FAIL:
      return {
        token: '',
        email: '',
        isLoading: false,
        hasError: true,
        shouldRedirect: false
      };

    default:
      return state;
  }

}

export default sessionReducer;

import sessionReducer, { INITIAL_STATE } from './session.reducer';
import * as types from './action-types';

const EMAIL = '__EMAIL__';
const TOKEN = '__TOKEN__';
const UNKNOWN_ACTION_TYPE = '__UNKNOWN_ACTION_TYPE__';
const STATE = {
  token: '',
  email: '',
  isLoading: false,
  hasError: false,
  shouldRedirect: false
};

describe('Session Reducer', () => {
  it('should set loading to true on SESSION_LOGIN action', () => {
    const nextState = sessionReducer(
      STATE,
      { type: types.SESSION_LOGIN }
    );

    expect(nextState.isLoading).toBeTruthy();
  });

  it('should set token and email on SESSION_LOGIN_SUCCESS action', () => {
    const nextState = sessionReducer(
      STATE,
      { type: types.SESSION_LOGIN_SUCCESS, token: TOKEN, email: EMAIL }
    );

    expect(nextState.token).toEqual(TOKEN);
    expect(nextState.email).toEqual(EMAIL);
    expect(nextState.shouldRedirect).toBeTruthy();
  });

  it('should set hasError and clears other properties on SESSION_LOGIN_FAIL action', () => {
    const nextState = sessionReducer(
      STATE,
      { type: types.SESSION_LOGIN_FAIL }
    );

    expect(nextState.token).toEqual('');
    expect(nextState.email).toEqual('');
    expect(nextState.isLoading).toBeFalsy();
    expect(nextState.hasError).toBeTruthy();
    expect(nextState.shouldRedirect).toBeFalsy();
  });

  it('should return unchanged state for unhandled action', () => {
    const nextState = sessionReducer(STATE, { type: UNKNOWN_ACTION_TYPE });

    expect(nextState).toEqual(STATE);
  });

  it('should use own INITIAL_STATE when no state passed', () => {
    const nextState = sessionReducer(undefined, { type: UNKNOWN_ACTION_TYPE });

    expect(nextState).toEqual(INITIAL_STATE);
  });
});

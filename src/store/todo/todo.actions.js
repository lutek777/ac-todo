import * as types from './action-types';

export function addItem(text) {
  return { type: types.TODO_ITEM_ADD, text };
}

export function deleteItem(index) {
  return { type: types.TODO_ITEM_DELETE, index };
}

export function editItem(index, todo) {
  return { type: types.TODO_ITEM_EDIT, index, todo };
}

export function sortByName(desc = false) {
  return desc ?
    { type: types.TODO_ITEM_SORT_NAME_DESC } :
    { type: types.TODO_ITEM_SORT_NAME };
}

export function sortByStatus(desc = false) {
  return desc ?
    { type: types.TODO_ITEM_SORT_STATUS_DESC } :
    { type: types.TODO_ITEM_SORT_STATUS };
}

export function startAddingItem() {
  return { type: types.TODO_ADDING_ITEM };
}

export function hideDone() {
  return { type: types.TODO_DONE_TOGGLE };
}



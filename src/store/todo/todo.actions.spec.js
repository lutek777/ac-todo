import * as types from './action-types';
import * as actions from './todo.actions';

const TEXT = '__TEXT__';
const INDEX = '__INDEX__';
const TODO = '__TODO__';

describe('Todo Actions', () => {
  describe('addItem', () => {
    it('should create TODO_ITEM_ADD action', () => {
      expect(actions.addItem(TEXT)).toEqual({
        type: types.TODO_ITEM_ADD,
        text: TEXT
      });
    });
  });

  describe('deleteItem', () => {
    it('should create TODO_ITEM_DELETE action', () => {
      expect(actions.deleteItem(INDEX)).toEqual({
        type: types.TODO_ITEM_DELETE,
        index: INDEX
      });
    });
  });

  describe('editItem', () => {
    it('should create TODO_ITEM_EDIT action', () => {
      expect(actions.editItem(INDEX, TODO)).toEqual({
        type: types.TODO_ITEM_EDIT,
        index: INDEX,
        todo: TODO
      });
    });
  });

  describe('sortByName', () => {
    it('should create TODO_ITEM_SORT_NAME', () => {
      expect(actions.sortByName()).toEqual({
        type: types.TODO_ITEM_SORT_NAME
      });
    });

    it('should create TODO_ITEM_SORT_NAME_DESC if parametrized', () => {
      expect(actions.sortByName(true)).toEqual({
        type: types.TODO_ITEM_SORT_NAME_DESC
      });
    });
  });

  describe('sortByStatus', () => {
    it('should create TODO_ITEM_SORT_STATUS', () => {
      expect(actions.sortByStatus()).toEqual({
        type: types.TODO_ITEM_SORT_STATUS
      });
    });

    it('should create TODO_ITEM_SORT_STATUS_DESC if parametrized', () => {
      expect(actions.sortByStatus(true)).toEqual({
        type: types.TODO_ITEM_SORT_STATUS_DESC
      });
    });
  });

  describe('startAddingItem', () => {
    it('should create TODO_ADDING_ITEM', () => {
      expect(actions.startAddingItem()).toEqual({
        type: types.TODO_ADDING_ITEM
      });
    });
  });

  describe('hideDone', () => {
    it('should create TODO_DONE_TOGGLE', () => {
      expect(actions.hideDone()).toEqual({
        type: types.TODO_DONE_TOGGLE
      });
    });
  });
});

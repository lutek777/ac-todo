import {combineReducers} from 'redux';
import * as types from './action-types';

export const INITIAL_ADDING_STATE = false;
export const INITIAL_HIDE_STATE = false;
export const INITIAL_ITEM_STATE = [
  { done: true, text: 'Write code' },
  { done: false, text: 'Write tests' },
  { done: true, text: 'Run unit testing' },
  { done: false, text: 'Run UAT' },
  { done: false, text: 'Deploy to production' },
  { done: false, text: 'Inspect analytics' }
];

export function isAddingReducer(state = false, action) {
  switch (action.type) {
    case types.TODO_ADDING_ITEM:
      return true;

    case types.TODO_ITEM_ADD:
      return false;

    default:
      return state;
  }
}

export function hideDoneReducer(state = false, action) {
  if (action.type === types.TODO_DONE_TOGGLE) {
    return !state;
  }

  return state;
}

export function itemsReducer(state = INITIAL_ITEM_STATE, action) {
  switch (action.type) {
    case types.TODO_ITEM_ADD:
      return [...state, { done: false, text: action.text }];

    case types.TODO_ITEM_DELETE:
      return [
        ...state.slice(0, action.index),
        ...state.slice(action.index + 1)
      ];

    case types.TODO_ITEM_EDIT:
      return state.map((item, index) => {
        if (index === action.index) {
          item = action.todo;
        }

        return item;
      });

    case types.TODO_ITEM_SORT_NAME:
      return [...state].sort((a, b) => {
        const c = a.text.toLowerCase();
        const d = b.text.toLowerCase();

        return (c < d) ? -1 : (c > d) ? 1 : 0
      });

    case types.TODO_ITEM_SORT_NAME_DESC:
      return [...state].sort((a, b) => {
        const c = a.text.toLowerCase();
        const d = b.text.toLowerCase();

        return (c > d) ? -1 : (c < d) ? 1 : 0
      });

    case types.TODO_ITEM_SORT_STATUS:
      return [...state].sort((a, b) => {
        return (a.done < b.done) ? -1 : 1
      });

    case types.TODO_ITEM_SORT_STATUS_DESC:
      return [...state].sort((a, b) => {
        return (a.done > b.done) ? -1 : 1
      });

    default:
      return state;
  }

}

const todoReducer = combineReducers({
  isAdding: isAddingReducer,
  hideDone: hideDoneReducer,
  items: itemsReducer
});

export default todoReducer;

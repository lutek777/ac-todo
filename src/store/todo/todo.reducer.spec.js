import todoReducer, {
  hideDoneReducer,
  isAddingReducer,
  itemsReducer,
  INITIAL_ADDING_STATE,
  INITIAL_HIDE_STATE,
  INITIAL_ITEM_STATE } from './todo.reducer';
import * as types from './action-types';

const UNKNOWN_ACTION_TYPE = '__UNKNOWN_ACTION_TYPE__';
const ITEM_1 = { done: false, text: '__FIRST_TEXT__' };
const ITEM_2 = { done: true, text: '__SECOND_TEXT__' };
const ITEM_3 = { done: false, text: '__LAST_TEXT__' };
const ITEM_EDITED = '__ITEM_EDITED__';
const TEXT = '__TEXT__';

describe('Todo Reducer', () => {
  describe('isAdding Reducer', () => {
    it('should set state to true on TODO_ADDING_ITEM action', () => {
      const nextState = isAddingReducer(undefined, { type: types.TODO_ADDING_ITEM });

      expect(nextState).toEqual(true);
    });

    it('should set state to false on TODO_ITEM_ADD action', () => {
      const nextState = isAddingReducer(undefined, { type: types.TODO_ITEM_ADD });

      expect(nextState).toEqual(false);
    });
  });

  describe('hideDone Reducer', () => {
    it('should set toggle state on TODO_DONE_TOGGLE action', () => {
      let nextState = hideDoneReducer(true, { type: types.TODO_DONE_TOGGLE });

      expect(nextState).toEqual(false);

      nextState = hideDoneReducer(false, { type: types.TODO_DONE_TOGGLE });

      expect(nextState).toEqual(true);
    });
  });

  describe('items Reducer', () => {
    it('should add new todo to state on TODO_ITEM_ADD action', () => {
      let nextState = itemsReducer([ ITEM_1 ], {
        type: types.TODO_ITEM_ADD,
        text: TEXT
      });

      expect(nextState.length).toEqual(2);
      expect(nextState[1]).toEqual({ done: false, text: TEXT });
    });

    it('should delete todo from state on TODO_ITEM_DELETE action', () => {
      let nextState = itemsReducer([ ITEM_1, ITEM_2, ITEM_3 ], {
        type: types.TODO_ITEM_DELETE,
        index: 1
      });

      expect(nextState).toEqual([ ITEM_1, ITEM_3 ]);
    });

    it('should edit todo in state on TODO_ITEM_EDIT action', () => {
      let nextState = itemsReducer([ ITEM_1, ITEM_2, ITEM_3 ], {
        type: types.TODO_ITEM_EDIT,
        index: 0,
        todo: ITEM_EDITED
      });

      expect(nextState).toEqual([ ITEM_EDITED, ITEM_2, ITEM_3 ]);
    });

    it('should sort todos in state on TODO_ITEM_SORT_NAME action', () => {
      let nextState = itemsReducer([ ITEM_1, ITEM_2, ITEM_3 ], {
        type: types.TODO_ITEM_SORT_NAME
      });

      expect(nextState).toEqual([ ITEM_1, ITEM_3, ITEM_2 ]);
    });

    it('should sort todos in state on TODO_ITEM_SORT_NAME_DESC action', () => {
      let nextState = itemsReducer([ ITEM_1, ITEM_2, ITEM_3 ], {
        type: types.TODO_ITEM_SORT_NAME_DESC
      });

      expect(nextState).toEqual([ ITEM_2, ITEM_3, ITEM_1 ]);
    });

    it('should sort todos in state on TODO_ITEM_SORT_STATUS action', () => {
      let nextState = itemsReducer([ ITEM_1, ITEM_2, ITEM_3 ], {
        type: types.TODO_ITEM_SORT_STATUS
      });

      expect(nextState).toEqual([ ITEM_3, ITEM_1, ITEM_2 ]);
    });

    it('should sort todos in state on TODO_ITEM_SORT_STATUS_DESC action', () => {
      let nextState = itemsReducer([ ITEM_1, ITEM_2, ITEM_3 ], {
        type: types.TODO_ITEM_SORT_STATUS_DESC
      });

      expect(nextState).toEqual([ ITEM_2, ITEM_3, ITEM_1 ]);
    });


  });

  describe('todo Reducer', () => {
    it('should return initial state for unhandled action', () => {
      let nextState = todoReducer(undefined, { type: UNKNOWN_ACTION_TYPE });

      expect(nextState).toEqual({
        isAdding: INITIAL_ADDING_STATE,
        hideDone: INITIAL_HIDE_STATE,
        items: INITIAL_ITEM_STATE
      });
    });
  });
});
